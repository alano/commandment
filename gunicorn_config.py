import os

port = '8080'
if os.environ.get('COMMANDMENT_PORT'):
    try:
        port = int(os.environ.get('COMMANDMENT_PORT'))
    except ValueError:
        port = int(os.environ.get('COMMANDMENT_PORT').split(':')[-1])

bind = "0.0.0.0:%s" % port
workers = 1
reload = bool(os.environ.get('COMMANDMENT_DEBUG'))
